#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <memory>

#include "../../src/headers/Commands/EndRotateCommand.h"

#include "../Capabilities/Mock_IInstructable.h"
#include "../../src/headers/ICommand.h"

using ::testing::_;

TEST(EndRotateCommand_test, execute_test)
{
    Mock_IInstructable iInstr;

    EXPECT_CALL(iInstr, set_command("rotateCommand", _));

    EndRotateCommand command(iInstr);

    command.execute();
};
