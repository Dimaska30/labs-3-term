#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <memory.h>

#include "../Mock_ICommand.h"
#include "../../src/headers/Commands/MultipleCommand.h"

using namespace std;

static void setting(Mock_ICommand &command)
{
    EXPECT_CALL(command, execute())
        .Times(3);
};

TEST(MultipleCommand_test, execute_test)
{
    shared_ptr<Mock_ICommand> ptr = make_shared<Mock_ICommand>();

    setting(*ptr);

    MultipleCommand command({ptr, ptr, ptr});

    command.execute();
};
