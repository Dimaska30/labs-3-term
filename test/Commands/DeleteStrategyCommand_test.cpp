#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../../src/headers/Commands/DeleteStrategyCommand.h"

#include "../IoC/Mock_IDependenciesContainer.h"

using namespace std;

static void setting(Mock_IDependenciesContainer &container, string &str)
{
    EXPECT_CALL(container, pop(str))
        .Times(1);
};

TEST(DeleteStrategyCommand_test, execute_test)
{
    string str("any words");
    shared_ptr<Mock_IDependenciesContainer> ptr = make_shared<Mock_IDependenciesContainer>();

    setting(*ptr, str);

    DeleteStrategyCommand command(ptr, str);

    command.execute();
};
