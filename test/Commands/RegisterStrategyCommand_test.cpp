#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../IoC/Mock_IDependenciesContainer.h"
#include "../../src/headers/Commands/RegisterStrategyCommand.h"
#include "../../src/headers/Strategies/EmptyStrategy.h"
#include "../../src/headers/Strategies/IStrategy.h"

using ::testing::_;

void setting(Mock_IDependenciesContainer &container, std::string &str)
{
    EXPECT_CALL(container, set(str, _))
        .Times(1);
};

TEST(RegisterStrategyCommand_test, execute_test)
{
    std::string str("any words");
    std::shared_ptr<Mock_IDependenciesContainer> ptr = std::make_shared<Mock_IDependenciesContainer>();
    std::unique_ptr<IStrategy> strategy = std::make_unique<EmptyStrategy>();

    setting(*ptr, str);

    RegisterStrategyCommand command(ptr, str, std::move(strategy));

    command.execute();
};
