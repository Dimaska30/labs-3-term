#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <vector>
#include <memory>
#include <map>
#include <string>

#include "../../src/headers/Commands/StartRotateCommand.h"

#include "../Mock_ICommand.h"
#include "../Capabilities/Mock_IInstructable.h"
#include "../../src/headers/IoC/resolve.h"
#include "../../src/headers/IoC/DependenciesContainer.h"
#include "../../src/headers/IoC/IDependenciesContainer.h"
#include "../../src/headers/IoC/DependenciesContainerSingleton.h"
#include "../../src/headers/Strategies/HolderStrategy.h"
#include "../../src/headers/Strategies/VariableStrategy.h"
#include "../../src/headers/Strategies/ResolveStrategy.h"
#include "../../src/headers/ICommand.h"
#include "../../src/headers/Commands/EmptyCommand.h"
#include "../../src/headers/Events/ICommandChain.h"
#include "../../src/headers/Events/CommandChain.h"


using ::testing::Return;

using namespace std;

class StartRotateCommand_test : public ::testing::Test
{

protected:
    StartRotateCommand_test()
    {
        shared_ptr<IDependenciesContainer> container = make_shared<DependenciesContainer>();

        shared_ptr<IStrategy> strategy(new ResolveStrategy(container));

        shared_ptr<IStrategy> register_strategy(new HolderStrategy(container));

        vector<string> command{"Add"};

        map<string, any> arg{{"requiest", make_any<string>("Strategy")},
                             {"strategy", make_any<shared_ptr<IStrategy>>(register_strategy)}};

        shared_ptr<ICommand> regist_cmd = any_cast<shared_ptr<ICommand>>(
            register_strategy->perform({{"requiests", command},
                                        {"args", arg}}));

        regist_cmd->execute();

        arg = {{"requiest", make_any<string>("Resolve")},
               {"strategy", make_any<shared_ptr<IStrategy>>(strategy)}};

        regist_cmd = any_cast<shared_ptr<ICommand>>(
            register_strategy->perform(
                {{"requiests", command},
                 {"args", arg}}));

        regist_cmd->execute();

        DependenciesContainerSingleton::setContainer(container);

        strategy = make_shared<HolderStrategy>();
        regist_cmd = resolve<shared_ptr<ICommand>>("Strategy.Add",
                                                   {{"requiest", make_any<string>("Command")},
                                                    {"strategy", strategy}});

        regist_cmd->execute();

        shared_ptr<ICommandChain> cmd_chain = make_shared<CommandChain>();

        shared_ptr<IStrategy> temp_strategy(new VariableStrategy<ICommandChain>(cmd_chain));
        regist_cmd = resolve<shared_ptr<ICommand>>("Command.Add",
                                                   {{"requiest", make_any<string>("Chain")},
                                                    {"strategy", temp_strategy}});

        regist_cmd->execute();
    };

    ~StartRotateCommand_test() = default;
};

TEST_F(StartRotateCommand_test, execute_test)
{
    shared_ptr<ICommandChain> commandChain_ptr = resolve<shared_ptr<ICommandChain>>("Command.Chain.Get", {});
    shared_ptr<Mock_ICommand> mockCommand = make_shared<Mock_ICommand>();
    shared_ptr<ICommand> empty = make_shared<EmptyCommand>();
    string path("moveCommand");

    Mock_IInstructable instr;

    shared_ptr<ICommand> cmd(new StartRotateCommand(mockCommand, instr));

    cmd->execute();
    EXPECT_EQ(1, commandChain_ptr->getSize());
};
