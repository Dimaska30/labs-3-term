#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <memory>

#include "../../src/headers/Commands/EndMoveCommand.h"

#include "../Capabilities/Mock_IInstructable.h"
#include "../../src/headers/ICommand.h"

using ::testing::_;

TEST(EndMoveCommand_test, execute_test)
{
    Mock_IInstructable iInstr;

    EXPECT_CALL(iInstr, set_command("moveCommand", _));

    EndMoveCommand command(iInstr);

    command.execute();
};
