#include "gtest/gtest.h"

#include <any>
#include <memory>
#include <vector>
#include <map>
#include <string>

#include "../../src/headers/Strategies/FunctionStrategy.h"
#include "../../src/headers/Strategies/EmptyStrategy.h"
#include "../../src/headers/IoC/IDependenciesContainer.h"
#include "../IoC/Mock_IDependenciesContainer.h"

static void setting1(Mock_IDependenciesContainer &container, std::string &str)
{
    EXPECT_CALL(container, get(str))
        .Times(1);
};

static void setting2(Mock_IDependenciesContainer &container, std::string &str)
{
    EXPECT_CALL(container, get(str))
        .Times(1)
        .WillOnce(testing::Return(make_shared<EmptyStrategy>()));
};

TEST(FunctionStrategy_test, constructor_with_params_test)
{
    shared_ptr<Mock_IDependenciesContainer> container = make_shared<Mock_IDependenciesContainer>();
    string str("any string");

    setting1(*container, str);

    FunctionStrategy strategy(container);

    ((strategy.get_container()).lock())->get(str);
};

TEST(FunctionStrategy_test, constructor_without_params_test)
{
    shared_ptr<IDependenciesContainer> container;

    FunctionStrategy strategy;

    EXPECT_NO_THROW(container = (strategy.get_container()).lock());
};

TEST(FunctionStrategy_test, perform_test)
{
    shared_ptr<Mock_IDependenciesContainer> container = make_shared<Mock_IDependenciesContainer>();
    string str("any string");
    vector<string> req{str, str, str};
    map<string, any> args;

    setting2(*container, str);

    FunctionStrategy strategy(container);

    any result;

    EXPECT_NO_THROW(result = (strategy.perform({{"requiests", req}, {"args", args}})));
};

TEST(FunctionStrategy_test, get_container_test)
{
    shared_ptr<IDependenciesContainer> container;

    FunctionStrategy strategy;

    EXPECT_NO_THROW(container = (strategy.get_container()).lock());
};