#include <iostream>
#include "gtest/gtest.h"

#include <vector>
#include <memory>
#include <map>
#include <string>
#include <any>

#include "../../src/headers/Strategies/HolderStrategy.h"
#include "../../src/headers/Strategies/IStrategy.h"
#include "../../src/headers/Strategies/EmptyStrategy.h"
#include "../../src/headers/ICommand.h"

#include "./PrintStrategy.h"

using namespace std;

class HolderStrategy_test : public ::testing::Test
{
protected:
    HolderStrategy strtgy;

    HolderStrategy_test() : strtgy(){};

    ~HolderStrategy_test() = default;
};

TEST_F(HolderStrategy_test, add_test)
{
    shared_ptr<IStrategy> anyStr = make_shared<EmptyStrategy>();
    string nameStr("Any");
    vector<string> req{"Add", "Del"};

    map<string, any> args{
        {"strategy", anyStr},
        {"requiest", nameStr}};

    shared_ptr<ICommand> result = any_cast<shared_ptr<ICommand>>(strtgy.perform({{"requiests", req}, {"args", args}}));
    EXPECT_NO_THROW(result->execute());
    req[0] = nameStr;
    result = any_cast<shared_ptr<ICommand>>(strtgy.perform({{"requiests", req}, {"args", args}}));
    EXPECT_NO_THROW(result->execute());
};

TEST_F(HolderStrategy_test, delete_test)
{
    shared_ptr<IStrategy> anyStr = make_shared<EmptyStrategy>();
    string nameStr("Any");
    vector<string> req{"Add", "Del"};

    map<string, any> args{
        {"strategy", anyStr},
        {"requiest", nameStr}};

    shared_ptr<ICommand> result = any_cast<shared_ptr<ICommand>>(strtgy.perform({{"requiests", req}, {"args", args}}));
    EXPECT_NO_THROW(result->execute());
    req[0] = "Delete";
    result = any_cast<shared_ptr<ICommand>>(strtgy.perform({{"requiests", req}, {"args", args}}));
    EXPECT_NO_THROW(result->execute());
    req[0] = nameStr;
    EXPECT_ANY_THROW(any_cast<shared_ptr<ICommand>>(strtgy.perform({{"requiests", req}, {"args", args}})));
};

TEST_F(HolderStrategy_test, change_test)
{
    shared_ptr<IStrategy> anyStr = make_shared<EmptyStrategy>();
    shared_ptr<IStrategy> otherStr = make_shared<PrintStrategy>();
    string nameStr("Any");
    vector<string> req{"Add", "Del"};

    map<string, any> args{
        {"strategy", anyStr},
        {"requiest", nameStr}};

    shared_ptr<ICommand> result = any_cast<shared_ptr<ICommand>>(strtgy.perform({{"requiests", req}, {"args", args}}));
    EXPECT_NO_THROW(result->execute());
    req[0] = "Change";
    args["strategy"] = make_any<shared_ptr<IStrategy>>(otherStr);
    result = any_cast<shared_ptr<ICommand>>(strtgy.perform({{"requiests", req}, {"args", args}}));
    EXPECT_NO_THROW(result->execute());
    req[0] = nameStr;
    args = {
        {"any", 53},
        {"words", 53},
        {"are", 53},
        {"printed", 54},
        {"in", 45},
        {"here", 65}};
    result = any_cast<shared_ptr<ICommand>>(strtgy.perform({{"requiests", req}, {"args", args}}));
    EXPECT_NO_THROW(result->execute());
};