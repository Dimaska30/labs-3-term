#include <iostream>
#include "gtest/gtest.h"

#include <vector>
#include <memory>
#include <map>
#include <string>
#include <any>

#include <iostream>

#include "../../src/headers/Strategies/VariableStrategy.h"
#include "../../src/headers/Strategies/IStrategy.h"
#include "../../src/headers/Strategies/EmptyStrategy.h"
#include "../../src/headers/ICommand.h"

#include "./PrintStrategy.h"

using namespace std;

class VariableStrategy_test : public ::testing::Test
{
protected:
    VariableStrategy<int> strtgy;

    VariableStrategy_test() : strtgy(make_shared<int>(10)){};

    ~VariableStrategy_test() = default;
};

TEST_F(VariableStrategy_test, get_test)
{
    vector<string> req{"Get", "Del"};
    map<string, any> args{
        {"strategy", make_shared<int>(15)},
        {"requiest", "set"}};
    int answ = *(any_cast<shared_ptr<int>>(strtgy.perform({{"requiests", req}, {"args", args}})));
    EXPECT_EQ(10, answ);
};

TEST_F(VariableStrategy_test, set_test)
{
    vector<string> req{"Set", "Del"};
    map<string, any> args{
        {"value", make_shared<int>(15)},
        {"requiest", "set"}};
    EXPECT_TRUE(any_cast<bool>(strtgy.perform({{"requiests", req}, {"args", args}})));
    req[0] = "Get";
    int answ = *(any_cast<shared_ptr<int>>(strtgy.perform({{"requiests", req}, {"args", args}})));
    EXPECT_EQ(15, answ);
};
