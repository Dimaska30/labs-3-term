#include "./PrintStrategy.h"

#include <any>
#include <string>
#include <map>

#include <memory>
#include <iostream>
#include <vector>

#include "../../src/headers/ICommand.h"
#include "../../src/headers/Commands/EmptyCommand.h"

std::any PrintStrategy::perform(std::map<std::string, std::any> args)
{
    std::vector<std::string> requiests = std::any_cast<std::vector<std::string>>(args.at("requiests"));
    std::map<std::string, std::any> main_arg = std::any_cast<std::map<std::string, std::any>>(args.at("args"));
    std::cout << "=== PrintStrategy ===\n";
    std::cout << "requiests:\n";
    for (const std::string &s : requiests)
        std::cout << s << ".";
    std::cout << "\n";
    std::cout << "args:\n";
    for (const auto &pair : main_arg)
        std::cout << pair.first << ", ";
    std::cout << "\n========= END =======\n";
    std::shared_ptr<ICommand> command = std::make_shared<EmptyCommand>();
    return std::make_any<std::shared_ptr<ICommand>>(command);
};
