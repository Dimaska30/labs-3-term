#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <vector>
#include <memory>

#include "../../src/headers/Strategies/ResolveStrategy.h"
#include "../../src/headers/IoC/DependenciesContainer.h"
#include "../../src/headers/IoC/IDependenciesContainer.h"
#include "../../src/headers/ICommand.h"
#include "./PrintStrategy.h"
#include "../../src/headers/Strategies/IStrategy.h"

TEST(ResolveStrategy_test, perform_test)
{
    std::shared_ptr<IDependenciesContainer> container = std::make_shared<DependenciesContainer>();
    std::shared_ptr<IStrategy> any_strategy = std::make_shared<PrintStrategy>();
    container->set("EmptyStrategy", any_strategy);
    ResolveStrategy strategy(container);
    std::shared_ptr<ICommand> command;
    std::map<std::string, std::any> any_args{
        {"any", 42},
        {"arguments", 42},
        {"are", 42},
        {"in", 42},
        {"here", 42}};
    auto temp = strategy.perform({{"requiest", std::string("EmptyStrategy.Get.Set.Delete")},
                                  {"args", any_args}});
    EXPECT_NO_THROW(command = std::any_cast<std::shared_ptr<ICommand>>(temp));
};
