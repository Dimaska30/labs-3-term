#pragma once

#include <any>
#include <string>
#include <map>

#include "../../src/headers/Strategies/IStrategy.h"

class PrintStrategy : public IStrategy
{
public:
    PrintStrategy() = default;
    ~PrintStrategy() = default;

    std::any perform(std::map<std::string, std::any> args);
};
