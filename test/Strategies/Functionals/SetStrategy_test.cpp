#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <memory>
#include <map>
#include <string>
#include <any>

#include "../../../src/headers/Strategies/Functionals/SetStrategy.h"

using namespace std;

TEST(SetStrategy_test, perform_test)
{
    shared_ptr<bool> value = make_shared<bool>(true);
    SetStrategy<bool> strategy(value);

    shared_ptr<bool> new_value = make_shared<bool>(false);
    map<string, any> args{
        {"value", new_value}};

    bool result;

    auto temp = strategy.perform({{"requiest", string("Other_strategy.add")},
                                  {"args", args}});
    EXPECT_NO_THROW(result = std::any_cast<bool>(temp));
    EXPECT_TRUE(result);
    EXPECT_FALSE(*value);
};
