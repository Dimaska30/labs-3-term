#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <vector>
#include <memory>
#include <map>
#include <string>
#include <any>

#include "../../../src/headers/Strategies/Functionals/AddStrategy.h"

#include "../../../src/headers/IoC/IDependenciesContainer.h"
#include "../../../src/headers/ICommand.h"
#include "../../../src/headers/Strategies/IStrategy.h"

#include "../../IoC/Mock_IDependenciesContainer.h"
#include "../../../src/headers/Strategies/EmptyStrategy.h"

using ::testing::_;

using namespace std;

static void setting(Mock_IDependenciesContainer &container, std::string &str)
{
    EXPECT_CALL(container, set(str, _))
        .Times(1);
};

TEST(AddStrategy_test, perform_test)
{
    shared_ptr<Mock_IDependenciesContainer> container = make_shared<Mock_IDependenciesContainer>();
    shared_ptr<IStrategy> any_strategy = make_shared<EmptyStrategy>();

    string nameStr("MyStrategy");

    setting(*container, nameStr);

    AddStrategy strategy(container);

    shared_ptr<ICommand> command_ptr;

    map<string, any> args{
        {"strategy", any_strategy},
        {"requiest", nameStr}};

    auto temp = strategy.perform({{"requiest", string("Other_strategy.add")},
                                  {"args", args}});
    EXPECT_NO_THROW(command_ptr = std::any_cast<std::shared_ptr<ICommand>>(temp));
    command_ptr->execute();
};
