#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <memory>
#include <map>
#include <string>
#include <any>

#include "../../../src/headers/Strategies/Functionals/GetStrategy.h"

using namespace std;

TEST(GetStrategy_test, perform_test)
{
    shared_ptr<bool> value = make_shared<bool>(true);
    GetStrategy<bool> strategy(value);

    map<string, any> args{
        {"requiest", "aaaaaaaaa"}};

    shared_ptr<bool> result;

    auto temp = strategy.perform({{"requiest", string("Other_strategy.add")},
                                  {"args", args}});
    EXPECT_NO_THROW(result = std::any_cast<shared_ptr<bool>>(temp));
    EXPECT_TRUE(result = value);
};
