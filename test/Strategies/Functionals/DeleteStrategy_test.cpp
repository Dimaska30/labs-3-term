#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <vector>
#include <memory>

#include "../../../src/headers/Strategies/Functionals/DeleteStrategy.h"

#include "../../../src/headers/IoC/IDependenciesContainer.h"
#include "../../../src/headers/ICommand.h"
#include "../../../src/headers/Strategies/IStrategy.h"

#include "../../IoC/Mock_IDependenciesContainer.h"
#include "../../../src/headers/Strategies/EmptyStrategy.h"

using ::testing::_;

using namespace std;

static void setting(Mock_IDependenciesContainer &container, std::string &str)
{
    EXPECT_CALL(container, pop(str))
        .Times(1);
};

TEST(DeleteStrategy_test, perform_test)
{
    shared_ptr<Mock_IDependenciesContainer> container = make_shared<Mock_IDependenciesContainer>();

    string nameStr("MyStrategy");

    setting(*container, nameStr);

    DeleteStrategy strategy(container);

    shared_ptr<ICommand> command_ptr;

    map<string, any> args{
        {"requiest", nameStr}};

    auto temp = strategy.perform({{"requiest", string("Other_strategy.add")},
                                  {"args", args}});
    EXPECT_NO_THROW(command_ptr = std::any_cast<std::shared_ptr<ICommand>>(temp));
    command_ptr->execute();
};
