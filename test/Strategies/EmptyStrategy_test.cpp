#include "gtest/gtest.h"

#include <any>
#include <memory>

#include "../../src/headers/Strategies/EmptyStrategy.h"
#include "../../src/headers/Commands/EmptyCommand.h"

TEST(EmptyStrategy_test, perform_test)
{

    EmptyStrategy strategy;

    std::shared_ptr<ICommand> command;

    EXPECT_NO_THROW(command = std::any_cast<std::shared_ptr<ICommand>>(strategy.perform({})));
    EXPECT_NO_THROW(command->execute());
    EXPECT_EQ(command.use_count(), 1);
};