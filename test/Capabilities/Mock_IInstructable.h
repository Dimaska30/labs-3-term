#pragma once

#include "gmock/gmock.h"

#include <memory>
#include <string>

#include "../../src/headers/Capabilities/IInstructable.h"
#include "../../src/headers/ICommand.h"

class Mock_IInstructable : public IInstructable
{
public:
    MOCK_METHOD(std::shared_ptr<ICommand>, get_command, (std::string name_cmd), (override));

    MOCK_METHOD(void, set_command, (std::string name_cmd, std::shared_ptr<ICommand> cmd), (override));
};
