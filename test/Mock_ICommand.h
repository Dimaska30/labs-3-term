#pragma once

#include "gmock/gmock.h"
#include "../src/headers/ICommand.h"

class Mock_ICommand : public ICommand
{
public:
    MOCK_METHOD(void, execute, (), (override));
};
