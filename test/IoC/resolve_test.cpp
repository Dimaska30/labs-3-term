#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <vector>
#include <memory>

#include "../../src/headers/IoC/resolve.h"
#include "../../src/headers/IoC/DependenciesContainer.h"
#include "../../src/headers/IoC/IDependenciesContainer.h"
#include "../../src/headers/IoC/DependenciesContainerSingleton.h"
#include "../../src/headers/Strategies/HolderStrategy.h"
#include "../../src/headers/Strategies/ResolveStrategy.h"
#include "../../src/headers/ICommand.h"

#include "../Strategies/PrintStrategy.h"

using sh_ptr_cmnd = std::shared_ptr<ICommand>;
using sh_ptr_strg = std::shared_ptr<IStrategy>;

class Resolve_test : public ::testing::Test
{

protected:
    Resolve_test()
    {
        std::shared_ptr<IDependenciesContainer> container = std::make_shared<DependenciesContainer>();

        sh_ptr_strg strategy(new ResolveStrategy(container));

        sh_ptr_strg register_strategy(new HolderStrategy(container));

        std::vector<std::string> command{"Add"};

        std::map<std::string, std::any> arg{{"requiest", std::make_any<std::string>("Strategy")},
                                            {"strategy", std::make_any<std::shared_ptr<IStrategy>>(register_strategy)}};

        std::shared_ptr<ICommand> registRegistr = std::any_cast<std::shared_ptr<ICommand>>(
            register_strategy->perform({{"requiests", command},
                                        {"args", arg}}));

        registRegistr->execute();

        arg = {{"requiest", std::make_any<std::string>("Resolve")},
               {"strategy", std::make_any<std::shared_ptr<IStrategy>>(strategy)}};

        std::shared_ptr<ICommand> registResolve = std::any_cast<std::shared_ptr<ICommand>>(
            register_strategy->perform(
                {{"requiests", command},
                 {"args", arg}}));

        registResolve->execute();

        DependenciesContainerSingleton::setContainer(container);
    };

    ~Resolve_test() = default;
};

TEST_F(Resolve_test, simple_test)
{
    sh_ptr_strg any_strategy = std::make_shared<PrintStrategy>();
    sh_ptr_cmnd registPrint = resolve<sh_ptr_cmnd>("Strategy.Add",
                                                   {{"requiest", std::make_any<std::string>("Print")},
                                                    {"strategy", any_strategy}});
    registPrint->execute();
    std::map<std::string, std::any>
        any_args{
            {"This", 13},
            {"is", 13},
            {"two", 13},
            {"call", 13},
            {"PrintStrategy", 13}};
    std::shared_ptr<ICommand> emptyCommand = resolve<std::shared_ptr<ICommand>>("Print", any_args);
};
