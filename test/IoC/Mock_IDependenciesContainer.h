#pragma once

#include <string>
#include <memory>

#include "gmock/gmock.h"
#include "../../src/headers/IoC/IDependenciesContainer.h"
#include "../../src/headers/Strategies/IStrategy.h"

class Mock_IDependenciesContainer : public IDependenciesContainer
{
public:
    MOCK_METHOD(void, set, (std::string key, std::shared_ptr<IStrategy> strategy), (override));

    MOCK_METHOD(std::weak_ptr<IStrategy>, get, (const std::string &key), (override));

    MOCK_METHOD(void, pop, (const std::string &key), (override));
};
