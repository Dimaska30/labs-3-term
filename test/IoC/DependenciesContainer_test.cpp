#include "gtest/gtest.h"

#include <memory>
#include <any>

#include "../../src/headers/IoC/DependenciesContainer.h"
#include "../../src/headers/Strategies/IStrategy.h"
#include "../../src/headers/Strategies/EmptyStrategy.h"
#include "../../src/headers/ICommand.h"

class DependenciesContainer_test : public ::testing::Test
{
protected:
    DependenciesContainer container;
    DependenciesContainer_test()
    {
        std::shared_ptr<IStrategy> one = std::make_shared<EmptyStrategy>();
        std::shared_ptr<IStrategy> two = std::make_shared<EmptyStrategy>();
        std::shared_ptr<IStrategy> empty;
        container.set("firstStrategy", one);
        container.set("secondStrategy", two);
        container.set("thridStategy", empty);
    };

    ~DependenciesContainer_test() = default;
};

TEST_F(DependenciesContainer_test, set_nullptr_test)
{
    EXPECT_NO_THROW(container.set("try", nullptr));
};

TEST_F(DependenciesContainer_test, set_empty_test)
{
    std::shared_ptr<IStrategy> empty;
    EXPECT_NO_THROW(container.set("try 2", empty));
};

TEST_F(DependenciesContainer_test, get_test)
{
    std::weak_ptr<IStrategy> strategy;
    EXPECT_NO_THROW(strategy = container.get("firstStrategy"));
    auto i_strategy = strategy.lock();
    EXPECT_EQ(i_strategy.use_count(), 2);
    auto command = std::any_cast<std::shared_ptr<ICommand>>(i_strategy->perform({}));
    command->execute();
};

TEST_F(DependenciesContainer_test, get_bad_test)
{
    std::weak_ptr<IStrategy> strategy;
    EXPECT_ANY_THROW(strategy = container.get("one"));
};

TEST_F(DependenciesContainer_test, pop_test)
{
    std::weak_ptr<IStrategy> strategy;
    EXPECT_NO_THROW(container.pop("firstStrategy"));
    EXPECT_ANY_THROW(strategy = container.get("firstStrategy"));
};

TEST_F(DependenciesContainer_test, pop__bad_test)
{
    EXPECT_ANY_THROW(container.pop("2"));
};
