#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <memory>
#include <any>

#include "../../src/headers/IoC/DependenciesContainerSingleton.h"
#include "../../src/headers/IoC/IDependenciesContainer.h"
#include "../../src/headers/Strategies/IStrategy.h"
#include "../../src/headers/Strategies/EmptyStrategy.h"
#include "../../src/headers/ICommand.h"
#include "./Mock_IDependenciesContainer.h"

class DependenciesContainerSingleton_test : public ::testing::Test
{
protected:
    Mock_IDependenciesContainer container;
    DependenciesContainerSingleton_test(){};

    ~DependenciesContainerSingleton_test() = default;
};

TEST_F(DependenciesContainerSingleton_test, get_test)
{
    std::weak_ptr<IDependenciesContainer> ptr = DependenciesContainerSingleton::getContainer();
    EXPECT_EQ(true, !ptr.expired());
    std::shared_ptr<IDependenciesContainer> work_ptr = ptr.lock();
    std::shared_ptr<IStrategy> temp = std::make_shared<EmptyStrategy>();
    work_ptr->set("one", temp);
};

TEST_F(DependenciesContainerSingleton_test, set_test)
{
    std::shared_ptr<IDependenciesContainer> arg = std::shared_ptr<Mock_IDependenciesContainer>();

    EXPECT_NO_THROW(DependenciesContainerSingleton::setContainer(std::move(arg)));
};