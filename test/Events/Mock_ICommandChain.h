#pragma once

#include <vector>
#include <memory>

#include "gmock/gmock.h"
#include "../../src/headers/Events/ICommandChain.h"
#include "../../src/headers/ICommand.h"

class Mock_ICommandChain : public ICommnadChain
{
public:
    MOCK_METHOD(std::vector<std::weak_ptr<ICommand>>, getIterate, (), (override));

    MOCK_METHOD(void, pushCommnads, (std::vector<std::weak_ptr<ICommand>> newCommnads), (override));
};
