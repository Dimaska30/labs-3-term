#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <vector>
#include <memory>

#include "../../src/headers/Events/EventLoop.h"

#include "../../src/headers/ICommand.h"
#include "../../src/headers/IoC/IDependenciesContainer.h"
#include "../../src/headers/IoC/DependenciesContainer.h"
#include "../../src/headers/IoC/DependenciesContainerSingleton.h"
#include "../../src/headers/Strategies/HolderStrategy.h"
#include "../../src/headers/Strategies/VariableStrategy.h"
#include "../../src/headers/Strategies/ResolveStrategy.h"
#include "../../src/headers/IoC/resolve.h"

using namespace std;

class EventLoop_test : public ::testing::Test
{

protected:
    EventLoop loop;
    EventLoop_test()
    {
        shared_ptr<IDependenciesContainer> container = make_shared<DependenciesContainer>();
        shared_ptr<IStrategy> register_strategy(new HolderStrategy(container));

        shared_ptr<IStrategy> resolve_str(new ResolveStrategy(container));
        vector<string> command{"Add"};

        map<string, any> arg{{"requiest", make_any<string>("Strategy")},
                             {"strategy", make_any<shared_ptr<IStrategy>>(register_strategy)}};

        shared_ptr<ICommand> registCmd = any_cast<shared_ptr<ICommand>>(
            register_strategy->perform({{"requiests", command},
                                        {"args", arg}}));

        registCmd->execute();

        arg = {{"requiest", make_any<string>("Resolve")},
               {"strategy", make_any<shared_ptr<IStrategy>>(resolve_str)}};

        registCmd = any_cast<shared_ptr<ICommand>>(
            register_strategy->perform(
                {{"requiests", command},
                 {"args", arg}}));

        registCmd->execute();

        DependenciesContainerSingleton::setContainer(container);

        shared_ptr<IStrategy> GameStrategy = make_shared<HolderStrategy>();
        arg["requiest"] = make_any<string>("Game");
        arg["strategy"] = make_any<shared_ptr<IStrategy>>(GameStrategy);
        registCmd = resolve<shared_ptr<ICommand>>("Strategy.Add", arg);
        registCmd->execute();

        shared_ptr<bool> hard = make_shared<bool>(false);
        shared_ptr<IStrategy> HardReset = make_shared<VariableStrategy<bool>>(hard);

        arg["requiest"] = make_any<string>("HardReset");
        arg["strategy"] = make_any<shared_ptr<IStrategy>>(HardReset);
        registCmd = resolve<shared_ptr<ICommand>>("Game.Add", arg);
        registCmd->execute();

        shared_ptr<IStrategy> ExcaptionStrgy = make_shared<HolderStrategy>();
        arg["requiest"] = make_any<string>("Exception");
        arg["strategy"] = make_any<shared_ptr<IStrategy>>(ExcaptionStrgy);
        registCmd = resolve<shared_ptr<ICommand>>("Strategy.Add", arg);
        registCmd->execute();
    };

    ~EventLoop_test() = default;
};

TEST_F(EventLoop_test, iterate_test){

};
