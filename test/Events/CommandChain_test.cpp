#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <vector>
#include <memory>

#include "../../src/headers/Events/CommandChain.h"
#include "../../src/headers/ICommand.h"

class CommandChain_test : public ::testing::Test
{

protected:
    CommandChain empty_chain;
    CommandChain full_chain;
    CommandChain full_chain_two;
    CommandChain full_chain_three;
    std::vector<std::weak_ptr<ICommand>> twoCommands;

    CommandChain_test() : empty_chain(), full_chain(), full_chain_two(2), full_chain_three(3)
    {
        std::weak_ptr<ICommand> one, two;
        twoCommands.push_back(one);
        twoCommands.push_back(two);
        full_chain.pushCommnads(twoCommands);
        full_chain_two.pushCommnads(twoCommands);
        full_chain_three.pushCommnads(twoCommands);
    };

    ~CommandChain_test() = default;
};

TEST_F(CommandChain_test, getIterate_empty_test)
{
    auto newCommand = empty_chain.getIterate();
    EXPECT_EQ(newCommand.size(), 0);
};

TEST_F(CommandChain_test, pushCommnads_empty_vector_test)
{
    auto newCommand = empty_chain.getIterate();
    EXPECT_NO_THROW(empty_chain.pushCommnads(newCommand));
};

TEST_F(CommandChain_test, pushCommnads_full_vector_test)
{
    EXPECT_NO_THROW(empty_chain.pushCommnads(twoCommands));
};

TEST_F(CommandChain_test, getIterate_1_command_test)
{
    auto newCommand = full_chain.getIterate();
    EXPECT_EQ(newCommand.size(), 1);
};

TEST_F(CommandChain_test, getIterate_2_command_test)
{
    auto newCommand = full_chain_two.getIterate();
    EXPECT_EQ(newCommand.size(), 2);
};

TEST_F(CommandChain_test, getIterate_2_command_when_iterate_3_test)
{
    auto newCommand = full_chain_three.getIterate();
    EXPECT_EQ(newCommand.size(), 2);
};
