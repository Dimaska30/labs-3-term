#pragma once

#include "gmock/gmock.h"
#include "../../src/headers/Events/IEventLoop.h"

class Mock_IEventLoop : public IEventLoop
{
public:
    MOCK_METHOD(void, iterate, (), (override));
};
