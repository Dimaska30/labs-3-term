#pragma once

#include <memory>

#include "./RepeatCommand.h"
#include "../ICommand.h"
#include "../Capabilities/IInstructable.h"
#include "../IoC/resolve.h"
#include "../Events/ICommandChain.h"

class StartRotateCommand : public ICommand
{
private:
    IInstructable &instructable;
    shared_ptr<ICommand> cmd;

public:
    StartRotateCommand(shared_ptr<ICommand> i_cmd, IInstructable &i_instructable) : cmd(i_cmd), instructable(i_instructable){};

    void execute();

    ~StartRotateCommand() = default;
};

void StartRotateCommand::execute()
{
    shared_ptr<ICommand> repeat(new RepeatCommand(cmd, instructable, "rotateCommand"));
    shared_ptr<ICommandChain> commandChain_ptr = resolve<shared_ptr<ICommandChain>>("Command.Chain.Get", {});
    commandChain_ptr->pushCommnads({repeat});
};
