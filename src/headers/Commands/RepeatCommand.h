#pragma once

#include <memory>
#include <string>

#include "../ICommand.h"
#include "../Capabilities/IInstructable.h"

using namespace std;

class RepeatCommand : public ICommand
{
private:
    shared_ptr<ICommand> cmd;
    string path;
    IInstructable &instr;

public:
    RepeatCommand(shared_ptr<ICommand> i_cmd, IInstructable &i_instr, string i_path);

    void execute();

    ~RepeatCommand() = default;
};
