#pragma once

#include <memory>

#include "../ICommand.h"
#include "../IoC/IDependenciesContainer.h"
#include "../Strategies/IStrategy.h"

class RegisterStrategyCommand : public ICommand
{
private:
    std::weak_ptr<IDependenciesContainer> container;
    std::string requiest;
    std::shared_ptr<IStrategy> strategy;

public:
    RegisterStrategyCommand(std::weak_ptr<IDependenciesContainer> i_container, std::string i_requiest, std::shared_ptr<IStrategy> i_strategy);

    virtual void execute();

    ~RegisterStrategyCommand() = default;
};
