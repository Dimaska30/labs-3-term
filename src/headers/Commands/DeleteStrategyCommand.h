#pragma once

#include <memory>

#include "../ICommand.h"
#include "../IoC/IDependenciesContainer.h"

using namespace std;

class DeleteStrategyCommand : public ICommand
{
private:
    weak_ptr<IDependenciesContainer> container;
    string requiest;

public:
    DeleteStrategyCommand(weak_ptr<IDependenciesContainer> i_container, string i_requiest);

    virtual void execute();

    ~DeleteStrategyCommand() = default;
};
