#pragma once

#include <memory>

#include "./EmptyCommand.h"
#include "../ICommand.h"
#include "../Capabilities/IInstructable.h"

class EndMoveCommand : public ICommand
{
private:
    IInstructable &instructable;

public:
    EndMoveCommand(IInstructable &i_instructable) : instructable(i_instructable){};

    void execute();

    ~EndMoveCommand() = default;
};

void EndMoveCommand::execute()
{
    std::shared_ptr<ICommand> empty_command = std::make_shared<EmptyCommand>();
    instructable.set_command("moveCommand", empty_command);
};
