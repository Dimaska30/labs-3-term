#pragma once

#include <memory>

#include "./EmptyCommand.h"
#include "../ICommand.h"
#include "../Capabilities/IInstructable.h"

class EndRotateCommand : public ICommand
{
private:
    IInstructable &instructable;

public:
    EndRotateCommand(IInstructable &i_instructable) : instructable(i_instructable){};

    void execute();

    ~EndRotateCommand() = default;
};

void EndRotateCommand::execute()
{
    std::shared_ptr<ICommand> empty_command = std::make_shared<EmptyCommand>();
    instructable.set_command("rotateCommand", empty_command);
};
