#pragma once

#include <memory>
#include <vector>

#include "../ICommand.h"

using namespace std;

class MultipleCommand : public ICommand
{
private:
    vector<shared_ptr<ICommand>> chain_cmd;

public:
    MultipleCommand(vector<shared_ptr<ICommand>> i_chain_cmd);

    virtual void execute();

    ~MultipleCommand() = default;
};
