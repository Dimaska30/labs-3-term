#pragma once

#include "../ICommand.h"

class EmptyCommand : public ICommand
{
private:
public:
    EmptyCommand() = default;

    virtual void execute(){};

    ~EmptyCommand() = default;
};
