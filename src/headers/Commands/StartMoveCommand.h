#pragma once

#include <memory>

#include "./RepeatCommand.h"
#include "../ICommand.h"
#include "../Capabilities/IInstructable.h"
#include "../IoC/resolve.h"
#include "../Events/ICommandChain.h"

using namespace std;

class StartMoveCommand : public ICommand
{
private:
    IInstructable &instructable;
    shared_ptr<ICommand> cmd;

public:
    StartMoveCommand(shared_ptr<ICommand> i_cmd, IInstructable &i_instructable) : cmd(i_cmd), instructable(i_instructable){};

    void execute();
};

void StartMoveCommand::execute()
{
    shared_ptr<ICommand> repeat(new RepeatCommand(cmd, instructable, "moveCommand"));
    shared_ptr<ICommandChain> commandChain_ptr = resolve<shared_ptr<ICommandChain>>("Command.Chain.Get", {});
    commandChain_ptr->pushCommnads({repeat});
};
