#pragma once

#include <any>
#include <map>
#include <string>

#include "./IStrategy.h"

class EmptyStrategy : public IStrategy
{
public:
    EmptyStrategy() = default;

    std::any perform(std::map<std::string, std::any> args);

    ~EmptyStrategy() = default;
};
