#pragma once

#include <any>
#include <map>
#include <string>
#include <memory>

#include "./IStrategy.h"
#include "./FunctionStrategy.h"
#include "./Functionals/GetStrategy.h"
#include "./Functionals/SetStrategy.h"
#include "../IoC/IDependenciesContainer.h"
#include "../IoC/DependenciesContainer.h"

using namespace std;

template <typename Type>
class VariableStrategy : public FunctionStrategy
{
private:
    shared_ptr<Type> value;

public:
    VariableStrategy(shared_ptr<IDependenciesContainer> i_container, shared_ptr<Type> default_value);

    VariableStrategy(shared_ptr<Type> default_value);

    any perform(map<string, any> args);

    ~VariableStrategy() = default;
};

template <typename Type>
VariableStrategy<Type>::VariableStrategy(shared_ptr<IDependenciesContainer> i_container, shared_ptr<Type> default_value) : FunctionStrategy(i_container), value(default_value)
{
    shared_ptr<IDependenciesContainer> container = (FunctionStrategy::get_container()).lock();
    shared_ptr<IStrategy> get(new GetStrategy(value));
    shared_ptr<IStrategy> set(new SetStrategy(value));
    container->set("Get", get);
    container->set("Set", set);
};

template <typename Type>
VariableStrategy<Type>::VariableStrategy(shared_ptr<Type> default_value) : VariableStrategy(make_shared<DependenciesContainer>(), default_value){};

template <typename Type>
any VariableStrategy<Type>::perform(map<string, any> args)
{
    return FunctionStrategy::perform(args);
};
