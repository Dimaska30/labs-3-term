#pragma once

#include <any>
#include <map>
#include <memory>
#include <string>

#include "./FunctionStrategy.h"
#include "../IoC/IDependenciesContainer.h"

using namespace std;

class HolderStrategy : public FunctionStrategy
{

public:
    HolderStrategy(shared_ptr<IDependenciesContainer> i_container);

    HolderStrategy();

    any perform(map<string, any> args);

    ~HolderStrategy() = default;
};