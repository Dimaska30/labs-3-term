#pragma once

#include <any>
#include <map>
#include <string>

class IStrategy
{
public:
    virtual std::any perform(std::map<std::string, std::any> args);

    virtual ~IStrategy() = default;
};

//Желательно, чтоб любая стратегия обрабатывала запросы:
// Add
// Delete
// Change