#pragma once

#include <any>
#include <map>
#include <memory>
#include <string>

#include "./IStrategy.h"
#include "../IoC/DependenciesContainer.h"

using namespace std;

class FunctionStrategy : public IStrategy
{
private:
    shared_ptr<IDependenciesContainer> container;

public:
    FunctionStrategy(shared_ptr<IDependenciesContainer> i_container);

    FunctionStrategy();

    virtual any perform(map<string, any> args);

    weak_ptr<IDependenciesContainer> get_container();

    ~FunctionStrategy() = default;

    enum RESULT
    {
        NOT_FOUND
    };
};