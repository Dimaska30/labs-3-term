#pragma once

#include <any>
#include <map>
#include <string>

#include "../IStrategy.h"
#include "../../IoC/IDependenciesContainer.h"

using namespace std;

class ChangeStrategy : public IStrategy
{
private:
    shared_ptr<IStrategy> addStrategy;
    shared_ptr<IStrategy> deleteStrategy;

public:
    ChangeStrategy(shared_ptr<IStrategy> i_addStrategy, shared_ptr<IStrategy> i_deleteStrategy);

    any perform(map<string, any> args);

    ~ChangeStrategy() = default;
};
