#pragma once

#include <any>
#include <map>
#include <string>

#include "../IStrategy.h"
#include "../../IoC/IDependenciesContainer.h"

using namespace std;

class DeleteStrategy : public IStrategy
{
private:
    shared_ptr<IDependenciesContainer> container;

public:
    DeleteStrategy(shared_ptr<IDependenciesContainer> i_container);

    any perform(map<string, any> args);

    ~DeleteStrategy() = default;
};
