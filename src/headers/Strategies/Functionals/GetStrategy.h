#pragma once

#include <any>
#include <map>
#include <string>
#include <memory>

#include "../IStrategy.h"

using namespace std;

template <typename T>
class GetStrategy : public IStrategy
{
private:
    shared_ptr<T> value;

public:
    GetStrategy(shared_ptr<T> i_value);

    any perform(map<string, any> args);

    ~GetStrategy() = default;
};

template <typename T>
GetStrategy<T>::GetStrategy(shared_ptr<T> i_value) : value(i_value){};

template <typename T>
any GetStrategy<T>::perform(map<string, any> args)
{
    return value;
};
