#pragma once

#include <any>
#include <map>
#include <string>
#include <memory>

#include "../IStrategy.h"

using namespace std;

template <typename T>
class SetStrategy : public IStrategy
{
private:
    shared_ptr<T> value;

public:
    SetStrategy(shared_ptr<T> i_value);

    any perform(map<string, any> args);

    ~SetStrategy() = default;
};

template <typename T>
SetStrategy<T>::SetStrategy(shared_ptr<T> i_value) : value(i_value){};

template <typename T>
any SetStrategy<T>::perform(map<string, any> args)
{
    map<string, any> main_args = any_cast<map<string, any>>(args.at("args"));
    shared_ptr<T> new_value = any_cast<shared_ptr<T>>(main_args.at("value"));
    *value = *new_value;
    return true;
};
