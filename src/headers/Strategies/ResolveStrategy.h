#pragma once

#include <any>
#include <map>
#include <memory>
#include <string>

#include "./IStrategy.h"
#include "../IoC/IDependenciesContainer.h"

class ResolveStrategy : public IStrategy
{
private:
    std::shared_ptr<IDependenciesContainer> container;

public:
    ResolveStrategy(std::shared_ptr<IDependenciesContainer> i_container);

    std::any perform(std::map<std::string, std::any> args);

    ~ResolveStrategy() = default;
};
