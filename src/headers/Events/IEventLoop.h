#pragma once

class IEventLoop
{
public:
    virtual void iterate() = 0;

    virtual ~IEventLoop() = default;
};
