#pragma once

#include "./IEventLoop.h"

class EventLoop : public IEventLoop
{
public:
    EventLoop() = default;

    virtual void iterate();

    ~EventLoop() = default;
};
