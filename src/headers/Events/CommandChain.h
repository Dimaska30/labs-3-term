#pragma once

#include <vector>
#include <queue>
#include <memory>

#include "./ICommandChain.h"
#include "../ICommand.h"

class CommandChain : public ICommandChain
{
private:
    std::queue<std::weak_ptr<ICommand>> storage;

    unsigned int count_iter_commands;

public:
    CommandChain();

    CommandChain(unsigned int n);

    virtual std::vector<std::weak_ptr<ICommand>> getIterate();

    virtual void pushCommnads(std::vector<std::weak_ptr<ICommand>> newCommnads);

    virtual unsigned int getSize();

    ~CommandChain() = default;
};
