#pragma once

#include <vector>
#include <memory>

#include "../ICommand.h"

class ICommandChain
{
public:
    virtual std::vector<std::weak_ptr<ICommand>> getIterate() = 0;

    virtual void pushCommnads(std::vector<std::weak_ptr<ICommand>> newCommnads) = 0;

    virtual unsigned int getSize() = 0;

    virtual ~ICommandChain() = default;
};
