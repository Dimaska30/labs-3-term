#pragma once
#include <string>
#include <memory>

#include "../ICommand.h"

class IInstructable
{
public:
    virtual std::shared_ptr<ICommand> get_command(std::string name_cmd) = 0;

    virtual void set_command(std::string name_cmd, std::shared_ptr<ICommand> cmd) = 0;

    virtual ~IInstructable() = default;
};
