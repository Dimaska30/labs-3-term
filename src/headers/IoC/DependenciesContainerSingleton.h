#pragma once

#include <memory>

#include "./IDependenciesContainer.h"

class DependenciesContainerSingleton
{
private:
    static std::shared_ptr<IDependenciesContainer> store;

    DependenciesContainerSingleton() = default;
    DependenciesContainerSingleton(const DependenciesContainerSingleton &a) = default;
    DependenciesContainerSingleton &operator=(DependenciesContainerSingleton &a) = default;

public:
    static std::weak_ptr<IDependenciesContainer> getContainer();

    static void setContainer(std::shared_ptr<IDependenciesContainer> container);
};
