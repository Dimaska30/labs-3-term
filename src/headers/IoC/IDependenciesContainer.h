#pragma once

#include <string>
#include <memory>

#include "../Strategies/IStrategy.h"

class IDependenciesContainer
{
public:
    virtual void set(std::string key, std::shared_ptr<IStrategy> strategy) = 0;

    virtual std::weak_ptr<IStrategy> get(const std::string &key) = 0;

    virtual void pop(const std::string &key) = 0;

    virtual ~IDependenciesContainer() = default;
};
