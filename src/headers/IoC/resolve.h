#pragma once

#include <any>
#include <map>
#include <string>
#include <memory>

#include "./IDependenciesContainer.h"
#include "./DependenciesContainerSingleton.h"

template <typename T>
T resolve(std::string request, std::map<std::string, std::any> args)
{
    std::weak_ptr<IDependenciesContainer> container = DependenciesContainerSingleton::getContainer();
    return std::any_cast<T>((((container.lock())->get("Resolve")).lock())->perform({{"requiest", request}, {"args", args}}));
};
