#pragma once

#include <map>
#include <string>
#include <memory>

#include "./IDependenciesContainer.h"
#include "../Strategies/IStrategy.h"

class DependenciesContainer : public IDependenciesContainer
{
private:
    std::map<std::string, std::shared_ptr<IStrategy>> store;

public:
    DependenciesContainer() = default;

    virtual void set(std::string key, std::shared_ptr<IStrategy> strategy);

    virtual std::weak_ptr<IStrategy> get(const std::string &key);

    virtual void pop(const std::string &key);

    ~DependenciesContainer() = default;
};
