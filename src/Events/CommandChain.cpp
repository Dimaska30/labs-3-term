#include "../headers/Events/CommandChain.h"

#include <vector>
#include <memory>

#include "../headers/ICommand.h"

CommandChain::CommandChain() : storage(), count_iter_commands(1){};

CommandChain::CommandChain(unsigned int n) : storage(), count_iter_commands(n){};

std::vector<std::weak_ptr<ICommand>> CommandChain::getIterate()
{
    std::vector<std::weak_ptr<ICommand>> result;
    for (int i = 0; i < count_iter_commands; i++)
        if (!storage.empty())
        {
            result.push_back(storage.front());
            storage.pop();
        }

    return result;
};

void CommandChain::pushCommnads(std::vector<std::weak_ptr<ICommand>> newCommnads)
{
    for (auto it = newCommnads.begin(); it != newCommnads.end();)
    {
        storage.push(*it);
        it++;
    }
};

unsigned int CommandChain::getSize()
{
    return storage.size();
};
