#include "../headers/Events/EventLoop.h"

#include <memory>

#include "../headers/Events/ICommandChain.h"
#include "../headers/IoC/resolve.h"
#include "../headers/ICommand.h"

void EventLoop::iterate()
{
    bool isDo = !resolve<bool>("Game.HardReset.Get", {});
    if (isDo)
    {
        std::shared_ptr<ICommandChain> commandChain_ptr = (resolve<std::weak_ptr<ICommandChain>>("Commands.Chain", {})).lock();
        std::vector<std::weak_ptr<ICommand>> commands = commandChain_ptr->getIterate();
        for (std::weak_ptr<ICommand> command : commands)
        {
            auto temp = command.lock();
            try
            {
                temp->execute();
            }
            catch (std::exception e)
            {
                bool result = resolve<bool>("Exception.Catch",
                                            {{"command", command},
                                             {"exception", e}});
            }
        }
    }
};
