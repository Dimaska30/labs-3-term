#include "../headers/Strategies/ResolveStrategy.h"

#include <any>
#include <map>
#include <memory>
#include <string>
#include <vector>
#include <sstream>

#include "../headers/Strategies/IStrategy.h"
#include "../headers/IoC/IDependenciesContainer.h"
#include "../headers/ICommand.h"

ResolveStrategy::ResolveStrategy(std::shared_ptr<IDependenciesContainer> i_container) : container(i_container){};

std::any ResolveStrategy::perform(std::map<std::string, std::any> args)
{
    std::string requiest = std::any_cast<std::string>(args.at("requiest"));
    auto main_arg = args.at("args");

    std::vector<std::string> requiests;
    std::istringstream f(requiest);
    std::string s;
    while (getline(f, s, '.'))
        requiests.push_back(s);

    s = requiests[0];
    requiests.erase(requiests.begin());

    return container->get(s).lock()->perform({{"requiests", requiests}, {"args", main_arg}});
};
