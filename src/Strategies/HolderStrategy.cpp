#include "../headers/Strategies/HolderStrategy.h"

#include "../headers/IoC/IDependenciesContainer.h"
#include "../headers/IoC/DependenciesContainer.h"
#include "../headers/Strategies/IStrategy.h"
#include "../headers/Strategies/Functionals/AddStrategy.h"
#include "../headers/Strategies/Functionals/DeleteStrategy.h"
#include "../headers/Strategies/Functionals/ChangeStrategy.h"

#include <any>
#include <map>
#include <string>

using namespace std;

HolderStrategy::HolderStrategy(shared_ptr<IDependenciesContainer> i_container) : FunctionStrategy(i_container)
{
    shared_ptr<IDependenciesContainer> container = (FunctionStrategy::get_container()).lock();
    shared_ptr<IStrategy> add = make_shared<AddStrategy>(container);
    shared_ptr<IStrategy> delete_ = make_shared<DeleteStrategy>(container);
    shared_ptr<IStrategy> change = make_shared<ChangeStrategy>(add, delete_);
    container->set("Add", add);
    container->set("Delete", delete_);
    container->set("Change", change);
};

HolderStrategy::HolderStrategy() : HolderStrategy(make_shared<DependenciesContainer>()){};

any HolderStrategy::perform(map<string, any> args)
{

    return FunctionStrategy::perform(args);
};
