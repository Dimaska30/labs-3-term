#include "../../headers/Strategies/Functionals/DeleteStrategy.h"

#include <memory>
#include <any>
#include <map>
#include <string>

#include "../../headers/IoC/IDependenciesContainer.h"
#include "../../headers/ICommand.h"
#include "../../headers/Commands/DeleteStrategyCommand.h"

using namespace std;

DeleteStrategy::DeleteStrategy(shared_ptr<IDependenciesContainer> i_container) : container(i_container){};

any DeleteStrategy::perform(map<string, any> args)
{
    map<string, any> main_args = any_cast<map<string, any>>(args.at("args"));
    string requiest = any_cast<string>(main_args.at("requiest"));
    shared_ptr<ICommand> result = make_shared<DeleteStrategyCommand>(container, requiest);
    return result;
};
