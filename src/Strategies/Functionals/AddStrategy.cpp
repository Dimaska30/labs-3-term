#include "../../headers/Strategies/Functionals/AddStrategy.h"

#include <memory>
#include <any>
#include <map>
#include <string>

#include "../../headers/IoC/IDependenciesContainer.h"
#include "../../headers/ICommand.h"
#include "../../headers/Commands/RegisterStrategyCommand.h"

using namespace std;

AddStrategy::AddStrategy(shared_ptr<IDependenciesContainer> i_container) : container(i_container){};

any AddStrategy::perform(map<string, any> args)
{
    map<string, any> main_args = any_cast<map<string, any>>(args.at("args"));
    shared_ptr<IStrategy> strategy = any_cast<shared_ptr<IStrategy>>(main_args.at("strategy"));
    string requiest = any_cast<string>(main_args.at("requiest"));
    shared_ptr<ICommand> result = make_shared<RegisterStrategyCommand>(container, requiest, strategy);
    return result;
};
