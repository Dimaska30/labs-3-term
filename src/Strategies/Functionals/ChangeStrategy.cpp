#include "../../headers/Strategies/Functionals/ChangeStrategy.h"

#include <memory>
#include <any>
#include <map>
#include <string>
#include <vector>

#include "../../headers/ICommand.h"
#include "../../headers/Strategies/IStrategy.h"
#include "../../headers/Commands/MultipleCommand.h"

using namespace std;

ChangeStrategy::ChangeStrategy(shared_ptr<IStrategy> i_addStrategy, shared_ptr<IStrategy> i_deleteStrategy) : addStrategy(i_addStrategy), deleteStrategy(i_deleteStrategy){};

any ChangeStrategy::perform(map<string, any> args)
{
    shared_ptr<ICommand> delete_cmd = any_cast<shared_ptr<ICommand>>(deleteStrategy->perform(args));
    shared_ptr<ICommand> add_cmd = any_cast<shared_ptr<ICommand>>(addStrategy->perform(args));
    vector<shared_ptr<ICommand>> cmds{delete_cmd, add_cmd};
    shared_ptr<ICommand> result = make_shared<MultipleCommand>(cmds);
    return result;
};
