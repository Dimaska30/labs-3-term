#include "../headers/Strategies/FunctionStrategy.h"

#include <any>
#include <map>
#include <memory>
#include <string>
#include <vector>
#include <exception>

#include "../headers/IoC/IDependenciesContainer.h"
#include "../headers/IoC/DependenciesContainer.h"

using namespace std;

FunctionStrategy::FunctionStrategy(shared_ptr<IDependenciesContainer> i_container) : container(i_container){};

FunctionStrategy::FunctionStrategy() : container(make_shared<DependenciesContainer>()){};

any FunctionStrategy::perform(map<string, any> args)
{
    any req;
    try
    {
        req = args.at("requiests");
    }
    catch (...)
    {
        throw "Don't find 'requiests'";
    };

    vector<string> str;
    try
    {
        str = any_cast<vector<string>>(req);
    }
    catch (...)
    {
        throw "field 'requiests' havn't expected type";
    };

    try
    {
        req = args.at("args");
    }
    catch (...)
    {
        throw "Don't find 'args'";
    };

    map<string, any> main_arg;
    try
    {
        main_arg = any_cast<map<string, any>>(req);
    }
    catch (...)
    {
        throw "field 'args' havn't expected type";
    };

    any result;
    string name_strgy;
    try
    {
        name_strgy = str[0];
        str.erase(str.begin());
        args["requiests"] = str;
    }
    catch (...)
    {
        throw "field 'requiests' is empty";
    }

    weak_ptr<IStrategy> strategy;

    try
    {
        strategy = container->get(name_strgy);
        shared_ptr<IStrategy> lock_strgy = strategy.lock();
        result = lock_strgy->perform(args);
    }
    catch (const exception &exc)
    {
        try
        {
            strategy = container->get("any");
            shared_ptr<IStrategy> lock_strgy = strategy.lock();
            result = lock_strgy->perform(args);
        }
        catch (...)
        {
            throw RESULT::NOT_FOUND;
        }
    }
    return result;
};

weak_ptr<IDependenciesContainer> FunctionStrategy::get_container()
{
    return container;
};
