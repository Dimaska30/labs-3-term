#include "../headers/Strategies/EmptyStrategy.h"

#include <any>
#include <map>
#include <string>

#include <memory>

#include "../headers/Commands/EmptyCommand.h"
#include "../headers/ICommand.h"

std::any EmptyStrategy::perform(std::map<std::string, std::any> args)
{
    std::shared_ptr<ICommand> command = std::make_shared<EmptyCommand>();
    return command;
};
