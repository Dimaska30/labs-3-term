#include "../headers/Strategies/IStrategy.h"

#include <any>
#include <map>
#include <string>
#include <memory>

#include "../headers/IoC/resolve.h"
#include "../headers/ICommand.h"

std::any IStrategy::perform(std::map<std::string, std::any> args)
{
    std::weak_ptr<ICommand> emptyCommand = resolve<std::weak_ptr<ICommand>>("Command.EmptyCommand.Get", {});
    return emptyCommand;
};
