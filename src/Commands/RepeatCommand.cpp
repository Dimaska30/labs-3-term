#include "../headers/Commands/RepeatCommand.h"

#include <memory>
#include <string>

#include "../headers/Capabilities/IInstructable.h"
#include "../headers/Events/ICommandChain.h"
#include "../headers/IoC/resolve.h"
#include "../headers/ICommand.h"

RepeatCommand::RepeatCommand(shared_ptr<ICommand> i_cmd, IInstructable &i_instr, string i_path) : cmd(i_cmd), instr(i_instr), path(i_path){};

void RepeatCommand::execute()
{
    cmd->execute();
    shared_ptr<ICommand> new_cmd = instr.get_command(path);
    shared_ptr<ICommandChain> commandChain_ptr = resolve<shared_ptr<ICommandChain>>("Command.Chain.Get", {});
    commandChain_ptr->pushCommnads({new_cmd});
};
