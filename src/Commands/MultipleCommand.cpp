#include "../headers/Commands/MultipleCommand.h"

#include <memory>
#include <vector>

#include "../headers/ICommand.h"

MultipleCommand::MultipleCommand(vector<shared_ptr<ICommand>> i_chain_cmd) : chain_cmd(i_chain_cmd){};

void MultipleCommand::execute()
{
    for (auto cmd : chain_cmd)
        cmd->execute();
};
