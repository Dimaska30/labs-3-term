#include "../headers/Commands/RegisterStrategyCommand.h"

#include <memory>

#include "../headers/IoC/IDependenciesContainer.h"
#include "../headers/Strategies/IStrategy.h"
#include "../headers/ICommand.h"

RegisterStrategyCommand::RegisterStrategyCommand(std::weak_ptr<IDependenciesContainer> i_container, std::string i_requiest, std::shared_ptr<IStrategy> i_strategy) : container(i_container), requiest(i_requiest), strategy(std::move(i_strategy)){};

void RegisterStrategyCommand::execute()
{
    std::shared_ptr<IDependenciesContainer> temp = container.lock();
    temp->set(requiest, strategy);
};
