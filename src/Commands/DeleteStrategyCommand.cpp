#include "../headers/Commands/DeleteStrategyCommand.h"

#include <memory>

#include "../headers/IoC/IDependenciesContainer.h"
#include "../headers/ICommand.h"

DeleteStrategyCommand::DeleteStrategyCommand(std::weak_ptr<IDependenciesContainer> i_container, std::string i_requiest) : container(i_container), requiest(i_requiest){};

void DeleteStrategyCommand::execute()
{
    std::shared_ptr<IDependenciesContainer> temp = container.lock();
    temp->pop(requiest);
};
