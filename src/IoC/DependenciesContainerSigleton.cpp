#include "../headers/IoC/DependenciesContainerSingleton.h"

#include <memory>

#include "../headers/IoC/IDependenciesContainer.h"
#include "../headers/IoC/DependenciesContainer.h"

std::shared_ptr<IDependenciesContainer> DependenciesContainerSingleton::store = std::make_shared<DependenciesContainer>();

std::weak_ptr<IDependenciesContainer> DependenciesContainerSingleton::getContainer()
{
    return store;
};

void DependenciesContainerSingleton::setContainer(std::shared_ptr<IDependenciesContainer> container)
{
    store = std::move(container);
};
