#include "../headers/IoC/DependenciesContainer.h"

#include <memory>

#include "../headers/Strategies/IStrategy.h"

void DependenciesContainer::set(std::string key, std::shared_ptr<IStrategy> strategy)
{
    if (store.count(key) != 0)
        throw "Strategy's name is busy";
    store.insert({key, strategy});
};

std::weak_ptr<IStrategy> DependenciesContainer::get(const std::string &key)
{
    if (store.count(key) == 0)
        throw "Strategy's name isn't found";
    std::shared_ptr<IStrategy> result = store.at(key);
    return result;
};

void DependenciesContainer::pop(const std::string &key)
{

    if (store.count(key) == 0)
        throw "Strategy's name isn't found";
    store.erase(key);
};
